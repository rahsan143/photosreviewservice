package com.etech.dataaccess;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

@Component
public class PhotosReviewDAO {
    private NamedParameterJdbcTemplate m_template;

    private static final double WGS84_a = 6378137.0;
    private static final double WGS84_b = 6356752.3;

    private static final double MIN_LAT = (Math.PI * -90) / 180.0;
    private static final double MAX_LAT = (Math.PI * 90) / 180.0;
    private static final double MIN_LON = (Math.PI * -180) / 180.0;
    private static final double MAX_LON = (Math.PI * 180) / 180.0;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        m_template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Autowired
    public String addPhoto(@Value("${addPhoto_insertSql}") String insertSql, @Value("${addPhoto_selectSql}") String selectSql,
                           String place_id, String baseFileName,
                           String caption, String username) {

        String fileName = null;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("place_id", place_id);
        params.addValue("username", username);
        params.addValue("caption", caption);
        params.addValue("baseFileName", baseFileName);

        KeyHolder key = new GeneratedKeyHolder();
        m_template.update(insertSql, params, key);

        int photo_id = key.getKey().intValue();

        params = new MapSqlParameterSource("photo_id", photo_id);

        try {
            fileName = m_template.queryForObject(selectSql, params, String.class);
        } catch (EmptyResultDataAccessException ex) {
            fileName = null;
        }

        return fileName;
    }

    @Autowired
    public boolean addReview(@Value("${addReview_selectSql}") String selectSql, @Value("${addReview_insertSql}") String insertSql,
                             @Value("${addReview_updateSql}") String updateSql,
                             String place_id, String username, ReviewElement re) {
        boolean update = false;
        boolean success = true;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("place_id", place_id);
            params.addValue("username", username);

            int review_id = m_template.queryForObject(selectSql, params,
                    Integer.class);

            if (review_id > 0) {
                update = true;
            }
        } catch (EmptyResultDataAccessException ex) {
            update = false;
        }

        if (!update) {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("place_id", place_id);
            params.addValue("username", username);
            params.addValue("rating", re.getRating());
            params.addValue("review", re.getData());

            success = (m_template.update(insertSql, params) == 1);
        } else {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("place_id", place_id);
            params.addValue("username", username);
            params.addValue("rating", re.getRating());
            params.addValue("review", re.getData());

            success = (m_template.update(updateSql, params) == 1);
        }

        return success;
    }

    public int getPlaceId(BusinessProfile profile, String username) {

        int ret_placeId = -1;

        int locationId = getLocationId(null, profile);

        // Decision tree to find/create place id for the correct business
        if (locationId >= 0) {
            // Location ID found
            int businessId = getBusinessId(null, profile, locationId);

            if (businessId >= 0) {
                // Business ID found
                ret_placeId = getBusinessPlaceId(null, profile, businessId);

                if (ret_placeId < 0) {
                    // Place Id not found so create placeId and then checkIn.
                    int cityId = getCityId(null, null, null, profile);
                    // if cityId is valid then proceed.
                    if (cityId >= 0) {
                        ret_placeId = createPlaceId(null, profile, cityId, locationId,
                                businessId, username);
                    }
                }
            } else {
                // Business ID not found
                businessId = createBusinessId(null, profile, locationId);
                // If businessId is valid then proceed.
                if (businessId >= 0) {
                    int cityId = getCityId(null, null, null, profile);
                    // If cityId is valid then proceed.
                    if (cityId >= 0) {
                        // Create placeId
                        ret_placeId = createPlaceId(null, profile, cityId,
                                locationId, businessId, username);
                    }
                }
            }
        } else {
            // Location ID not found
            // Check for Location IDs with a 250m range
            ArrayList<Integer> locationIdList = getLocationIdList(null, profile);
            if (locationIdList.size() > 0) {
                // Range of Ids found
                // For each of these ids find the corresponding businessIds that
                // matches the locationId
                ArrayList<Integer> businessIds = new ArrayList<Integer>();
                for (Integer locId : locationIdList) {
                    int businessId = getBusinessId(null, profile, locId);
                    if (businessId >= 0) {
                        businessIds.add(businessId);
                    }
                }

                if (businessIds.size() > 0) {
                    // Look for matching business Ids and business names in
                    // LOCAL_NEW_BUSINESS and return
                    // the corresponding place_ids
                    ArrayList<Integer> placeIds = new ArrayList<Integer>();
                    for (Integer busId : businessIds) {
                        int placeId = getBusinessPlaceId(null, profile, busId);
                        if (placeId >= 0) {
                            placeIds.add(placeId);
                        }
                    }

                    if (placeIds.size() > 0) {
                        // List of place ids obtained, now just use the first
                        // one to return back as there is no specific preference among
                        //them to choose from.
                        ret_placeId = placeIds.get(0);
                    } else {
                        // No matching place Ids found so create a new business
                        // altogether
                        int cityId = getCityId(null, null, null, profile);
                        if (cityId >= 0) {
                            locationId = createLocationId(null, profile);
                            if (locationId >= 0) {
                                int businessId = createBusinessId(null, profile,
                                        locationId);
                                if (businessId >= 0) {
                                    ret_placeId = createPlaceId(null, profile,
                                            cityId, locationId, businessId,
                                            username);

                                }
                            }
                        }
                    }
                } else {
                    // No business Ids found in range so create a new location and
                    // business altogether.
                    int cityId = getCityId(null, null, null, profile);
                    if (cityId >= 0) {
                        locationId = createLocationId(null, profile);
                        if (locationId >= 0) {
                            int businessId = createBusinessId(null, profile,
                                    locationId);
                            if (businessId >= 0) {
                                ret_placeId = createPlaceId(null, profile, cityId,
                                        locationId, businessId, username);
                            }
                        }
                    }
                }
            } else {
                // No location Ids found in range create place id.
                int cityId = getCityId(null, null, null, profile);
                if (cityId >= 0) {
                    locationId = createLocationId(null, profile);
                    if (locationId >= 0) {
                        int businessId = createBusinessId(null, profile, locationId);
                        if (businessId >= 0) {
                            ret_placeId = createPlaceId(null, profile, cityId,
                                    locationId, businessId, username);
                        }
                    }
                }

            }
        }

        return ret_placeId;
    }

    @Autowired
    public boolean businessProfileExists(@Value("${bpe_selectSql}") String selectSql, String placeId) {
        // Most latest RDBMS optimizes the count function so COUNT(*) would be
        // fine for checking record existence
        // However COUNT(1) is even better as it returns 1 if record exists, 0
        // otherwise.
        MapSqlParameterSource param = new MapSqlParameterSource("placeId",
                placeId);

        return m_template.queryForObject(selectSql, param, Integer.class) == 1;
    }

    @Autowired
    public int getCityId(@Value("${gci_select1Sql}") String select1Sql, @Value("${gci_select2Sql}") String select2Sql,
                         @Value("${gci_insertSql}") String insertSql, BusinessProfile profile) {
        String sql = "";

        // Setting cityId to -1 as the initial value, since cityId can never be
        // less that zero.
        int cityId = -1;

        if (profile.getState() == null) {
            sql = select1Sql;
        } else {
            sql = insertSql;
        }

        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(
                profile);

        Integer cityIdObject = null;

        try {
            cityIdObject = m_template
                    .queryForObject(sql, params, Integer.class);
            cityId = cityIdObject.intValue();
        } catch (EmptyResultDataAccessException ex) {
            cityIdObject = null;
        }

        if (cityIdObject == null) {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            m_template.update(insertSql, params, keyHolder);

            cityId = keyHolder.getKey().intValue();
        }

        return cityId;
    }

    @Autowired
    public int getLocationId(@Value("${gli_selectSql}") String selectSql, BusinessProfile profile) {
        // Setting location id to -1 as the initial value as location id can
        // never be less than 0.
        int locationId = -1;
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(
                profile);

        Integer locationIdObject = null;

        try {
            locationIdObject = m_template.queryForObject(selectSql, params,
                    Integer.class);
            locationId = locationIdObject.intValue();
        } catch (EmptyResultDataAccessException ex) {
            locationIdObject = null;
        }

        return locationId;
    }

    @Autowired
    public int getBusinessId(@Value("${gbi_selectSql}") String selectSql, BusinessProfile profile, int location_id) {
        // Setting business ID as -1 as the initial value as businessId can
        // never be less than 0.
        int businessId = -1;
        Integer businessIdObject = null;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("business_categories", profile.getBusiness_categories());
        params.addValue("location_id", location_id);


        try {
            businessIdObject = m_template.queryForObject(selectSql, params,
                    Integer.class);
            businessId = businessIdObject.intValue();
        } catch (EmptyResultDataAccessException ex) {
            businessIdObject = null;
        }

        return businessId;
    }

    @Autowired
    public int getBusinessPlaceId(@Value("${gbpi_selectSql") String selectSql, BusinessProfile profile, int businessId) {
        int placeId = -1;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("business_name", profile.getBusiness_name());
        params.addValue("business_id", businessId);

        Integer placeIdObject = null;

        try {
            placeIdObject = m_template.queryForObject(selectSql, params,
                    Integer.class);
            placeId = placeIdObject.intValue();
        } catch (EmptyResultDataAccessException ex) {
            placeIdObject = null;
        }

        return placeId;
    }

    @Autowired
    private int createLocationId(@Value("${cli_insertSql}") String insertSql, BusinessProfile profile) {
        int locationId = -1;

        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(
                profile);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        m_template.update(insertSql, params, keyHolder);

        locationId = keyHolder.getKey().intValue();

        return locationId;
    }

    @Autowired
    private int createBusinessId(@Value("${cbi_insertSql}") String insertSql, BusinessProfile profile, int location_id) {
        int businessId = -1;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("business_categories", profile.getBusiness_categories());
        params.addValue("location_id", location_id);

        KeyHolder keyHolder = new GeneratedKeyHolder();

        m_template.update(insertSql, params, keyHolder);

        businessId = keyHolder.getKey().intValue();

        return businessId;

    }

    @Autowired
    private int createPlaceId(@Value("${cpi_insertSql}") String insertSql, BusinessProfile profile, int cityId,
                              int locationId, int businessId, String username) {
        int placeId = -1;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("business_id", businessId);
        params.addValue("location_id", locationId);
        params.addValue("username", username);
        params.addValue("city_id", cityId);
        params.addValue("business_name", profile.getBusiness_name());
        params.addValue("street", profile.getStreet());
        params.addValue("postal_code", profile.getPostal_code());
        params.addValue("business_hours", profile.getBusiness_hours());
        params.addValue("phone", profile.getPhone());
        params.addValue("website", profile.getWebsite());
        params.addValue("google_place_id", profile.getGoogle_place_id());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        m_template.update(insertSql, params, keyHolder);

        placeId = keyHolder.getKey().intValue();

        return placeId;
    }

    @Autowired
    private ArrayList<Integer> getLocationIdList(@Value("${glil_selectSql") String selectSql, BusinessProfile profile) {
        ArrayList<Integer> locationList = new ArrayList<Integer>();

        GeoBoxElement geoBoxElement = createGeoBox(profile);

        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(
                geoBoxElement);

        List<Map<String, Object>> rows = m_template.queryForList(selectSql, params);

        for (Map<String, Object> row : rows) {
            locationList.add(Integer.parseInt("" + row.get("LOCATION_ID")));
        }

        return locationList;
    }

    private GeoBoxElement createGeoBox(BusinessProfile profile) {
        GeoBoxElement geo_box = null;

        // Converts latitude/longitude in degrees to radians. NOte a profile at
        // the very least should
        // have location coordinates(lat/lon), that's why not required to check
        // for null values in them.
        // If for some reason there is a null value an exception will be thrown
        // and recorded in the applications log.
        double lat_radians = (degreesToRadians(Double.parseDouble(profile
                .getLatitude().trim())));
        double lon_radians = (degreesToRadians(Double.parseDouble(profile
                .getLongitude().trim())));

        // This is the bounding distance for the geo box from the provided
        // location coordiantes.
        double distanceInKm = 0.25;
        double halfSide = 1000 * distanceInKm;

        // Radius of Earth at given latitude.
        double radius = WGS84EarthRadiusAtLatitude(lat_radians);
        double radDist = (halfSide / radius);

        double minLat = lat_radians - radDist;
        double maxLat = lat_radians + radDist;

        double minLon = 0;
        double maxLon = 0;

        if (minLat > MIN_LAT && maxLat < MAX_LAT) {
            // calculate longitude differential based on distance provided.
            double deltaLon = Math.asin(Math.sin(radDist)
                    / Math.cos(lat_radians));

            minLon = lon_radians - deltaLon;

            if (minLon < MIN_LON) {
                minLon += 2 * Math.PI;
            }

            maxLon = lon_radians + deltaLon;

            if (maxLon > MAX_LON) {
                maxLon -= 2 * Math.PI;
            }
        } else {
            minLat = Math.max(minLat, MIN_LAT);
            maxLat = Math.max(maxLat, MAX_LAT);

            minLon = MIN_LON;
            maxLon = MAX_LON;
        }

        minLat = radiansToDregrees(minLat);
        maxLat = radiansToDregrees(maxLat);

        minLon = radiansToDregrees(minLon);
        maxLon = radiansToDregrees(maxLon);

        geo_box = new GeoBoxElement();

        geo_box.setMin_latitude(minLat);
        geo_box.setMax_latitude(maxLat);

        geo_box.setMin_longitude(minLon);
        geo_box.setMax_longitude(maxLon);

        geo_box.setRadius(radius);

        return geo_box;
    }

    // Convert Degrees to Radians
    private double degreesToRadians(double degrees) {
        return (Math.PI * degrees) / 180.0;
    }

    // Convert Radians to Degrees
    private double radiansToDregrees(double radians) {
        return (radians * 180) / Math.PI;
    }

    /*
     * Earth radius at a given latitude, according to the WGS-84 ellipsoid.
     */
    private double WGS84EarthRadiusAtLatitude(double latitude) {
        double An = WGS84_a * WGS84_a * Math.cos(latitude);
        double Bn = WGS84_b * WGS84_b * Math.sin(latitude);
        double Ad = WGS84_a * Math.cos(latitude);
        double Bd = WGS84_b * Math.sin(latitude);

        return Math.sqrt((An * An + Bn * Bn) / (Ad * Ad + Bd * Bd));
    }
}
