package com.etech.dataaccess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "business_profile")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessProfile 
{
	@XmlElement
	private String place_id;
	
	@XmlElement
	private String google_place_id;
	
	@XmlElement
	private String business_name;
	
	@XmlElement
	private String address;
	
	@XmlElement
	private String phone;
	
	@XmlElement
	private String business_categories;
	
	@XmlElement
	private String business_hours;
	
	@XmlElement
	private String website;
	
	@XmlElement
	private String latitude;
	
	@XmlElement
	private String longitude;
	
	@XmlElement
	private String street;
	
	@XmlElement
	private String city;
	
	@XmlElement
	private String state;
	
	@XmlElement
	private String country;
	
	@XmlElement
	private String postal_code;

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBusiness_categories() {
		return business_categories;
	}

	public void setBusiness_categories(String business_categories) {
		this.business_categories = business_categories;
	}

	public String getBusiness_hours() {
		return business_hours;
	}

	public void setBusiness_hours(String business_hours) {
		this.business_hours = business_hours;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getGoogle_place_id() {
		return google_place_id;
	}

	public void setGoogle_place_id(String google_place_id) {
		this.google_place_id = google_place_id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
}
