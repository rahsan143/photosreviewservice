package com.etech.dataaccess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class encapsulates a Photo element, sent from the client
 * in the form of an XML doc.
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */

@XmlRootElement(name = "photo")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhotoElement 
{
	@XmlElement
	private String data;
	
	@XmlElement
	private String caption;

	public String getData() 
	{
		return data;
	}

	public void setData(String data) 
	{
		this.data = data;
	}

	public String getCaption() 
	{
		return caption;
	}

	public void setCaption(String caption) 
	{
		this.caption = caption;
	}

	
}
