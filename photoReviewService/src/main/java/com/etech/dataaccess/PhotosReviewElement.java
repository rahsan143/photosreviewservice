package com.etech.dataaccess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "photos_review")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhotosReviewElement 
{
	@XmlElement
	private String place_id;
	
	private BusinessProfile business_profile;
	
	private ReviewElement review;
	
	private PhotoElement photo;
	
	@XmlElement
	private String photo_tag;


	public String getPlace_id() 
	{
		return place_id;
	}

	public void setPlace_id(String place_id) 
	{
		this.place_id = place_id;
	}

	public ReviewElement getReview() 
	{
		return review;
	}

	public void setReview(ReviewElement review) 
	{
		this.review = review;
	}
	
	public PhotoElement getPhoto() 
	{
		return photo;
	}

	public void setPhoto(PhotoElement photo) 
	{
		this.photo = photo;
	}

	public BusinessProfile getBusiness_profile() {
		return business_profile;
	}

	public void setBusiness_profile(BusinessProfile business_profile) {
		this.business_profile = business_profile;
	}

	public String getPhoto_tag() {
		return photo_tag;
	}

	public void setPhoto_tag(String photo_tag) {
		this.photo_tag = photo_tag;
	}
}
