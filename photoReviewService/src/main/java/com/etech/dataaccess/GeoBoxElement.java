package com.etech.dataaccess;

public class GeoBoxElement {

	private double radius;

	private double min_latitude;

	private double min_longitude;

	private double max_latitude;

	private double max_longitude;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getMin_latitude() {
		return min_latitude;
	}

	public void setMin_latitude(double min_latitude) {
		this.min_latitude = min_latitude;
	}

	public double getMin_longitude() {
		return min_longitude;
	}

	public void setMin_longitude(double min_longitude) {
		this.min_longitude = min_longitude;
	}

	public double getMax_latitude() {
		return max_latitude;
	}

	public void setMax_latitude(double max_latitude) {
		this.max_latitude = max_latitude;
	}

	public double getMax_longitude() {
		return max_longitude;
	}

	public void setMax_longitude(double max_longitude) {
		this.max_longitude = max_longitude;
	}
}