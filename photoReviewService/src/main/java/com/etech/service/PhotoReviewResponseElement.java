package com.etech.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * PhotoReviewResponseElement.
 * Forms the following XML document when marshalled:
 * 
 * <photoreview_response>
 * 		<result>...</result>
 * 		<place_id>...</place_id>
 * 		<photo_url>...</photo_url>
 * 		<photo_tag>...</photo_tag>
 * </photoreview_response>
 * 
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */

@XmlRootElement(name = "photoreview_response")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhotoReviewResponseElement 
{
	@XmlElement
	private String result;
	
	@XmlElement
	private String place_id;
	
	@XmlElement 
	String photo_url;
	
	@XmlElement
	String photo_tag;

	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public String getPhoto_tag() {
		return photo_tag;
	}

	public void setPhoto_tag(String photo_tag) {
		this.photo_tag = photo_tag;
	}
}
