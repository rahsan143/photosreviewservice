package com.etech.service;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * MediaBodyWriter for PhotoReviewResponseElement.
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */

@Provider
@Produces(MediaType.APPLICATION_XML)
public class PhotoReviewResponseWriter implements  MessageBodyWriter<PhotoReviewResponseElement>
{

	@Override
	public boolean isWriteable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) 
	{
		return type.isAnnotationPresent(XmlRootElement.class);
	}

	@Override
	public long getSize(PhotoReviewResponseElement t, Class<?> type,
			Type genericType, Annotation[] annotations, MediaType mediaType) 
	{
		return -1;
	}

	@Override
	public void writeTo(PhotoReviewResponseElement prre, Class<?> type,
			Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders,
			OutputStream entityStream) throws IOException,
			WebApplicationException 
	{
		try 
		{
			JAXBContext context = JAXBContext.newInstance(type);
			context.createMarshaller().marshal(prre, entityStream);
		} 
		catch (JAXBException e) 
		{
			throw new RuntimeException(e);
		}
	}

}
