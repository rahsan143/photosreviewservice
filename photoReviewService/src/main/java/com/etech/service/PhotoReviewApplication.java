package com.etech.service;

/**
 * This class is the main entry point for the JAX-RS application.
 * It is loaded first into the application server.
 * 
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 */

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class PhotoReviewApplication extends Application 
{
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> empty = new HashSet<Class<?>>();
	
	public PhotoReviewApplication()
	{
		singletons.add(new PhotoReviewService());
	}
	
	@Override
	public Set<Class<?>> getClasses()
	{
		return empty;
	}
	
	@Override
	public Set<Object> getSingletons()
	{
		return singletons;
	}
}
