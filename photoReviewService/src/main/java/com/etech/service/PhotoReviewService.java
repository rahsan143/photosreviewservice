package com.etech.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.jboss.resteasy.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DeadlockLoserDataAccessException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.etech.dataaccess.PhotosReviewDAO;
import com.etech.dataaccess.PhotosReviewElement;
import com.etech.dataaccess.ReviewElement;
import com.etech.security.UserAuthenticated;
import org.springframework.stereotype.Component;

/**
 * This class provides the service of adding photo(s) and review to local database.
 * Please note even though this service might seem to allow photo and review submit
 * within the same XML document but it is best practice to submit photo and review
 * separately.
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */

@Path("/photoReview")
@Component
public class PhotoReviewService 
{
	private ApplicationContext m_context = null;
	private PhotosReviewDAO m_photosReviewDAO = null;
	
	private static final String BUSINESS_PHOTO_SUBMIT_SUCCESS = "BUSINESS_PHOTO_SUBMIT_SUCCESS";
	private static final String BUSINESS_PHOTO_SUBMIT_FAILED = "BUSINESS_PHOTO_SUBMIT_FAILED";
	
	private static final String BUSINESS_REVIEW_SUBMIT_SUCCESS = "BUSINESS_REVIEW_SUBMIT_SUCCESS";
	private static final String BUSINESS_REVIEW_SUBMIT_FAILED = "BUSINESS_REVIEW_SUBMIT_FAILED";
	Logger logger = (Logger) Logger.getLogger(PhotoReviewService.class);
	
	public PhotoReviewService()
	{
		m_context = new ClassPathXmlApplicationContext("bean.xml");
		
		m_photosReviewDAO = (PhotosReviewDAO)m_context.getBean("photosReviewDAO");
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/getStatus")
	@Autowired
	public String getStatus(@Value("${bucketName}") String bucketName, @Value("${key}") String key)
	{
		String resp = "";

		//Hardcoded values for debugging purposes.

		
		try
		{
			AmazonS3 s3Client = new AmazonS3Client(new InstanceProfileCredentialsProvider());
			
			S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucketName, key));
			
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));
			String line = "";
			while((line = buffReader.readLine()) != null)
			{
				resp += line;
			}
			
			buffReader.close();
		}
		 catch(AmazonServiceException ase)
	    {
	      logger.error("AmazonServiceException!", ase);
	    }
	    catch(AmazonClientException ace)
	    {
	      logger.error("AmazonClientException!", ace);
	    } catch (IOException e) {
			
			logger.error("IOException!", e);
		}
		
		return resp;
	}
	

	/**
	 * Method responsible for receiving photos/review and uploading it to the database.
	 * @param pre PhotosReviewElement unmarshalled from client's XML document
	 * @return Response object depicting either operation success or failure.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	@Path("/submitPhotosAndReview")
	@UserAuthenticated
	public Response submitPhotosAndReview(@Value("${bucketNameSubmit}") String bucketName, @Value("${keySubmit}") String keySubmit, @Value("${cloudFrontUrl}") String cloudFrontUrl, PhotosReviewElement pre, @HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader)
	{
		Response resp = null;
		String username = null;
		
		try 
		{
			String[] array = authorizationHeader.split(";");
			username = array[0];
			
			username = new String(Base64.decode(username), "UTF-8");
			if(pre.getPlace_id() != null)
			{
				pre.setPlace_id(new String(Base64.decode(pre.getPlace_id()), "UTF-8"));
			}
			else if(pre.getBusiness_profile() != null)
			{
				//Convert all values from base64 to normal text.
				if(pre.getBusiness_profile().getPlace_id() != null)
				{
					pre.getBusiness_profile().setPlace_id(new String(Base64.decode(pre.getBusiness_profile().getPlace_id()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getGoogle_place_id() != null)
				{
					pre.getBusiness_profile().setGoogle_place_id(new String(Base64.decode(pre.getBusiness_profile().getGoogle_place_id()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getBusiness_name() != null)
				{
					pre.getBusiness_profile().setBusiness_name(new String(Base64.decode(pre.getBusiness_profile().getBusiness_name()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getAddress() != null)
				{
					pre.getBusiness_profile().setAddress(new String(Base64.decode(pre.getBusiness_profile().getAddress()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getPhone() != null)
				{
					pre.getBusiness_profile().setPhone(new String(Base64.decode(pre.getBusiness_profile().getPhone()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getBusiness_categories() != null)
				{
					pre.getBusiness_profile().setBusiness_categories(new String(Base64.decode(pre.getBusiness_profile().getBusiness_categories()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getBusiness_hours() != null)
				{
					pre.getBusiness_profile().setBusiness_hours(new String(Base64.decode(pre.getBusiness_profile().getBusiness_hours()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getWebsite() != null)
				{
					pre.getBusiness_profile().setWebsite(new String(Base64.decode(pre.getBusiness_profile().getWebsite()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getLatitude() != null)
				{
					pre.getBusiness_profile().setLatitude(new String(Base64.decode(pre.getBusiness_profile().getLatitude()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getLongitude() != null)
				{
					pre.getBusiness_profile().setLongitude(new String(Base64.decode(pre.getBusiness_profile().getLongitude()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getStreet() != null)
				{
					pre.getBusiness_profile().setStreet(new String(Base64.decode(pre.getBusiness_profile().getStreet()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getCity() != null)
				{
					pre.getBusiness_profile().setCity(new String(Base64.decode(pre.getBusiness_profile().getCity()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getState() != null)
				{
					pre.getBusiness_profile().setState(new String(Base64.decode(pre.getBusiness_profile().getState()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getCountry() != null)
				{
					pre.getBusiness_profile().setCountry(new String(Base64.decode(pre.getBusiness_profile().getCountry()), "UTF-8"));
				}
				
				if(pre.getBusiness_profile().getPostal_code() != null)
				{
					pre.getBusiness_profile().setPostal_code(new String(Base64.decode(pre.getBusiness_profile().getPostal_code()), "UTF-8"));
				}
				
				pre.setPlace_id("" + m_photosReviewDAO.getPlaceId(pre.getBusiness_profile(), username));
			}
		} 
		catch (Exception ex) 
		{
			logger.error("Exception", ex);
			if(pre.getPhoto() != null)
			{
				resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(BUSINESS_PHOTO_SUBMIT_FAILED).build();
			}
			else if(pre.getReview() != null)
			{
				resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(BUSINESS_REVIEW_SUBMIT_FAILED).build();
			}
			
			return resp;
		} 
		
		if(pre.getPhoto() != null)
		{
			if(m_photosReviewDAO.businessProfileExists(null, pre.getPlace_id()))
			{
				//This is prefix or bucket address for cloudfront, the content delivery network.
				String filePrefix = "photo_" + pre.getPlace_id() + "_";
				
				String fileName =  null;
				String photo_url = null;
				try
				{
					fileName = m_photosReviewDAO.addPhoto(null, null, pre.getPlace_id(), (cloudFrontUrl + filePrefix), pre.getPhoto().getCaption(), username);
				}
				catch(DeadlockLoserDataAccessException de)
				{
					//Retry transaction due to deadlock.
					fileName = m_photosReviewDAO.addPhoto(null, null, pre.getPlace_id(), (cloudFrontUrl + filePrefix), pre.getPhoto().getCaption(), username);
				}
				
				photo_url = fileName;
				
				if(fileName != null)
				{
					fileName = fileName.replaceAll(cloudFrontUrl, "");

					//Hardcoded for debugging purposes.
					String key = keySubmit + fileName;
				
					boolean uploadSuccess;
				
					//This is to test from local computer
					//AmazonS3 s3Client = new AmazonS3Client(new com.amazonaws.auth.profile.ProfileCredentialsProvider());
				
					//This is to run from an EC2 instance
					AmazonS3 s3Client = new AmazonS3Client(new InstanceProfileCredentialsProvider());
					try
					{
						byte[] imageBytes = Base64.decode(pre.getPhoto().getData());
						InputStream in = new ByteArrayInputStream(imageBytes);
					
						//This is the minimum metadata needed to upload the file to S3. S3 requires the size of the object being
						//uploaded at minimum.
						ObjectMetadata objectMetaData = new ObjectMetadata();
						objectMetaData.setContentLength(imageBytes.length);
					
						PutObjectRequest por = new PutObjectRequest(bucketName, key, in, objectMetaData);
						
						//Set the read access to Public so that the content is available to anyone on the web.
						por.setCannedAcl(CannedAccessControlList.PublicRead);
					
						s3Client.putObject(por);
					
						uploadSuccess = true;
					}
					catch (AmazonServiceException ase) 
					{
						logger.error("Caught an AmazonServiceException, which " +
								"means your request made it " +
								"to Amazon S3, but was rejected with an error response" +
								" for some reason.");
						logger.error("Error Message:    " + ase.getMessage());
						logger.error("HTTP Status Code: " + ase.getStatusCode());
						logger.error("AWS Error Code:   " + ase.getErrorCode());
						logger.error("Error Type:       " + ase.getErrorType());
						logger.error("Request ID:       " + ase.getRequestId());
		            
						uploadSuccess = false;
					} 
					catch (AmazonClientException ace) 
					{
						logger.error("Caught an AmazonClientException, which " +
								"means the client encountered " +
								"an internal error while trying to " +
								"communicate with S3, " +
								"such as not being able to access the network.");
						logger.error("Error Message: " + ace.getMessage());
		            
						uploadSuccess = false;
					} catch (IOException e) 
					{
						logger.error("IOException!", e);
						uploadSuccess = false;
					}
			
					if(uploadSuccess)
					{
						PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
						prre.setResult(BUSINESS_PHOTO_SUBMIT_SUCCESS);
						prre.setPlace_id(pre.getPlace_id());
						if(photo_url != null)
						{
							prre.setPhoto_url(photo_url);
						}
						
						prre.setPhoto_tag(pre.getPhoto_tag());
						
						resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
					}
					else
					{
						PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
						prre.setResult(BUSINESS_PHOTO_SUBMIT_FAILED);
						resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
					}
				}
				else
				{
					PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
					prre.setResult(BUSINESS_PHOTO_SUBMIT_FAILED);
					resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
				}
			}
			else
			{
				PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
				prre.setResult(BUSINESS_PHOTO_SUBMIT_FAILED);
				resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
			}
		}
		else if(pre.getReview() != null)
		{
			if(m_photosReviewDAO.businessProfileExists(null, pre.getPlace_id()))
			{
				ReviewElement re = pre.getReview();
				try 
				{
					if(m_photosReviewDAO.addReview(null, null, null, pre.getPlace_id(), username, re))
					{
						PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
						prre.setResult(BUSINESS_REVIEW_SUBMIT_SUCCESS);
						prre.setPlace_id(pre.getPlace_id());
						resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
					}
					else
					{
						PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
						prre.setResult(BUSINESS_REVIEW_SUBMIT_FAILED);
						resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
					}
				}
				catch(Exception ex)
				{
					logger.error("Exception!", ex);
					
					PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
					prre.setResult(BUSINESS_REVIEW_SUBMIT_FAILED);
					resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
				}
			}
			else
			{
				PhotoReviewResponseElement prre = new PhotoReviewResponseElement();
				prre.setResult(BUSINESS_REVIEW_SUBMIT_FAILED);
				resp = Response.status(HttpStatus.SC_OK).type(MediaType.APPLICATION_XML).entity(prre).build();
			}
			
		}
		
		return resp;
	}
}