package com.etech.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * Special annotation for user authentication before using any service.
 * @author Rajib Ahsan
 * Copyright (c) 2015 eTech. All rights reserved.
 */

@NameBinding
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UserAuthenticated {

}
