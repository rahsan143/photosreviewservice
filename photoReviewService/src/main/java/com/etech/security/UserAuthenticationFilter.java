package com.etech.security;

import java.io.IOException;
import java.security.Principal;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.http.HttpStatus;


/**
 * UserAuthenticationFilter class filters each request before the web service performs any operation.
 * 
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */

@UserAuthenticated
@Priority(Priorities.AUTHENTICATION)
@Produces(MediaType.TEXT_PLAIN)
public class UserAuthenticationFilter implements ContainerRequestFilter 
{
	/*
	 * Filters incoming Request by authenticating them first and then passing them to the appropriate service.
	 * (non-Javadoc)
	 * @see javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.ContainerRequestContext)
	 */
	public void filter(ContainerRequestContext requestContext)
			throws IOException 
	{
		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if(authorizationHeader == null)
		{
			throw new NotAuthorizedException(Response.status(HttpStatus.SC_UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity("User Forbidden").build());
		}
		
		String[] array = authorizationHeader.split(";");
		
		if(array.length != 2)
		{
			throw new NotAuthorizedException(Response.status(HttpStatus.SC_UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity("User Forbidden").build());
		}
		
		final String user = array[0];
		String password = array[1];
		
		boolean success = false;
		try
		{
			success = UserAuthenticator.authenticatePassword(password);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(!success)
		{
			throw new NotAuthorizedException(Response.status(HttpStatus.SC_UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity("User Forbidden").build());
		}
		
		final SecurityContext securityContext = requestContext.getSecurityContext();
		requestContext.setSecurityContext(new SecurityContext(){

			public Principal getUserPrincipal() {
				
				return new Principal()
				{

					public String getName() 
					{
						
						return user;
					}
					
				};
			}

			public boolean isUserInRole(String role) 
			{
				return false;
			}

			public boolean isSecure() 
			{
				return securityContext.isSecure();
			}

			public String getAuthenticationScheme() {
				
				return "USER";
			}
			
		});
	}
}
