package com.etech.security;

import org.jboss.resteasy.util.Base64;
/**
 * Class that authenticates whether a password sent in by the user is authentic or not.
 * @author Rajib Ahsan
 * Copyright (c) 2014 eTech. All rights reserved.
 *
 */
public class UserAuthenticator 
{
	//Ideally the following properties should be in a properties file.
	//Hard coded for easier understanding.
	private static final String ENCRYPTION_KEY = "etech_general_encryption";
	private static final String PASSWORD_SUFFIX = "blue_local_user";
	private static final long MINUTES_3 = (3 * 60 * 1000); //3 mins
	
	/**
	 * Static class authenticates a password sent by the remote user.
	 * @param password Base64 encoded, encrypted password string.
	 * @return true if the password is valid, false otherwise.
	 * @throws Exception
	 */
	public static boolean authenticatePassword(String password) throws Exception
	{
		boolean isCorrect = false;
		
		Cipher cipher = new Cipher(ENCRYPTION_KEY);
		
		byte[] passBytes = cipher.decrypt(Base64.decode(password));
		
	    password = new String(passBytes);
	    
	    String[] array = password.split(":");
	    
	    if((array.length == 2) && array[0].equals(PASSWORD_SUFFIX))
	    {
	    	double userTime = Double.parseDouble(array[1]);
	    	double currentTime = System.currentTimeMillis();
	    	
	    	if((currentTime - userTime) <= MINUTES_3)
	    	{
	    		isCorrect = true;
	    	}
	    } 
		
		return isCorrect;
	}
}
